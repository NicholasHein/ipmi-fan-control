use clap::{Parser, Subcommand};

use crate::profiles::ProfileFactory;

#[derive(Parser)]
#[clap(author, version, about)]
pub struct Args {
    /// Subcommands
    #[clap(subcommand)]
    pub command: Command,

    /// Verbose output
    #[clap(long)]
    pub verbose: bool,
}

#[derive(Subcommand)]
pub enum Command {
    /// Auto adjust fan speed by interval checking CPU temperature
    Auto(Auto),

    /// Set fixed RPM percentage for fan
    Fixed(Fixed),

    /// Reset/disable manual fan control
    Reset,

    /// Print CPU temperature and fan RPM
    Info,
}

#[derive(clap::Args)]
pub struct Auto {
    /// check CPU temperature interval second
    #[clap(short, long, default_value = "5")]
    pub interval: u64,

    /// threshold CPU temperature for full speed Fan, default 70 (degrees), accepted value range [60-100]
    #[clap(short, long, default_value = "70")]
    pub threshold: u16,

    #[clap(short, long, value_parser = fan_profile_parser)]
    pub profile: ProfileFactory,
}

#[derive(clap::Args)]
pub struct Fixed {
    /// value range 0-100
    #[clap(value_parser)]
    pub value: u16,

    #[clap(short, long, value_parser = fan_profile_parser)]
    pub profile: ProfileFactory,
}

fn fan_profile_parser(s: &str) -> Result<ProfileFactory, String> {
    s.try_into()
        .map_err(|errstr| format!("`{}`: {}", s, errstr))
}
