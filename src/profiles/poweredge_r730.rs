use regex::Regex;
use std::fmt::{Display, Formatter};

use super::{CpuTemp, FanProfile, FanSpeed, SpeedProfile, TempProfile};

pub const NAME: &'static str = "poweredge-r730";

pub struct Profile;

impl TempProfile for Profile {
    fn get_temp(&mut self, ipmi_data: &str) -> Result<CpuTemp, super::Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"(?i)(\d*)\sdegrees").unwrap();
        }

        for x in ipmi_data.lines() {
            if x.to_lowercase().starts_with("temp ")
                || x.to_lowercase().starts_with("ambient temp ")
            {
                if let Some(v) = RE.captures(x) {
                    let tmp = v.get(1).unwrap().as_str().parse::<u16>()?;
                    return Ok(tmp);
                }
            }
        }

        Err(anyhow!("not found"))
    }
}

impl SpeedProfile for Profile {
    fn get_speed(&self, temp: CpuTemp) -> Option<FanSpeed> {
        let speed = match temp {
            0..=40 => 0.0, // 0%
            41..=50 => 0.02,
            51..=55 => 0.10,
            56..=60 => 0.30,
            61..=62 => 0.40,
            63..=65 => 0.50,
            66..=70 => 0.90,
            71..=100 => 1.0, // 100%
            _ => return None,
        };
        Some(speed)
    }
}

impl Display for Profile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", NAME)
    }
}

impl FanProfile for Profile {}
