use std::fmt::Display;

#[cfg(feature = "poweredge-r610")]
mod poweredge_r610; // Optional

mod poweredge_r730;

pub type CpuTemp = u16;
pub type FanSpeed = f32;

pub type Error = anyhow::Error;

#[derive(Clone)]
pub enum ProfileFactory {
    PoweredgeR610,
    PoweredgeR730,
}

pub trait FanProfile
where
    Self: TempProfile + SpeedProfile + Display,
{
}

pub trait TempProfile {
    fn get_temp(&mut self, ipmi_data: &str) -> Result<CpuTemp, Error>;
}

pub trait SpeedProfile {
    fn get_speed(&self, temp: CpuTemp) -> Option<FanSpeed>;
}

impl TryFrom<&str> for ProfileFactory {
    type Error = &'static str;
    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let name = value.to_lowercase();
        match &name[..] {
            #[cfg(feature = "poweredge-r610")]
            poweredge_r610::NAME => Ok(ProfileFactory::PoweredgeR610),
            poweredge_r730::NAME => Ok(ProfileFactory::PoweredgeR730),
            _ => Err("not a profile"),
        }
    }
}

impl Into<Box<dyn FanProfile>> for ProfileFactory {
    fn into(self) -> Box<dyn FanProfile> {
        match self {
            #[cfg(feature = "poweredge-r610")]
            ProfileFactory::PoweredgeR610 => Box::new(poweredge_r610::Profile::new()),
            ProfileFactory::PoweredgeR730 => Box::new(poweredge_r730::Profile),
        }
    }
}

pub trait HumanPercent {
    const MIN_HUMAN: u16 = 0;
    const MAX_HUMAN: u16 = 100;
    const MIN_PERCENT: f32 = 0.0;
    const MAX_PERCENT: f32 = 1.0;
    const HUMAN_COEF: f32 = 100.0;

    fn to_human(&self) -> f32;
    fn from_human(num: u16) -> Result<FanSpeed, &'static str>;
}

impl HumanPercent for FanSpeed {
    fn to_human(&self) -> f32 {
        self.clamp(Self::MIN_PERCENT, Self::MAX_PERCENT) * Self::HUMAN_COEF
    }

    fn from_human(num: u16) -> Result<FanSpeed, &'static str> {
        match num {
            num @ Self::MIN_HUMAN..=Self::MAX_HUMAN => Ok((num as FanSpeed) / Self::HUMAN_COEF),
            _ => Err("out of bounds (0 - 100)"),
        }
    }
}
