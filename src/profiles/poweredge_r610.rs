use std::{
    f32::consts::PI,
    fmt::{Display, Formatter},
};
use sysinfo::{Component, ComponentExt, RefreshKind, System, SystemExt};

use super::{CpuTemp, FanProfile, FanSpeed, SpeedProfile, TempProfile};

pub const NAME: &'static str = "poweredge-r610";

pub struct Profile {
    sys: System,
}

impl Profile {
    pub fn new() -> Profile {
        Profile {
            sys: System::new_with_specifics(RefreshKind::new().with_components_list()),
        }
    }

    pub fn foreach_temp<F>(&self, mut f: F) -> Result<(), super::Error>
    where
        F: FnMut(&Component) -> Result<(), super::Error>,
    {
        for comp in self.sys.components() {
            if !comp.label().to_lowercase().starts_with("core ") {
                continue;
            }
            f(&comp)?;
        }
        Ok(())
    }

    pub fn foreach_temp_refreshed<F>(&mut self, f: F) -> Result<(), super::Error>
    where
        F: FnMut(&Component) -> Result<(), super::Error>,
    {
        self.sys.refresh_components();
        self.foreach_temp(f)
    }
}

impl TempProfile for Profile {
    fn get_temp(&mut self, _ipmi_data: &str) -> Result<CpuTemp, super::Error> {
        let mut hitemp: Option<f32> = None;

        self.foreach_temp_refreshed(|comp| {
            let temp = comp.temperature().round();
            if let Some(crit_temp) = comp.critical().and_then(|crit_temp| {
                if crit_temp > temp {
                    None
                } else {
                    Some(crit_temp)
                }
            }) {
                return Err(anyhow!(
                    "'{}' temperature ({} C) exceeds critical level ({} C)",
                    comp.label(),
                    temp,
                    crit_temp
                ));
            }

            let max = comp.max();
            if max <= temp {
                return Err(anyhow!(
                    "'{}' temperature ({} C) exceeds max level ({} C)",
                    comp.label(),
                    temp,
                    max
                ));
            }

            hitemp = hitemp.or_else(|| Some(temp));
            if temp > hitemp.unwrap() {
                hitemp = Some(temp);
            }
            Ok(())
        })?;

        hitemp
            .map(|temp| temp.round() as CpuTemp)
            .ok_or_else(|| anyhow!("no temperature readings"))
    }
}

fn temp2speed_curve(temp: f32) -> f32 {
    const RELAX: f32 = 0.83; // [0, 1)

    // Let 0 <= x <= 1 and 0 <= a < PI/2
    // f(x) = k * tan(a * x)
    //    => k = 1 / tan(a)
    // f(x) = tan(a * x) / tan(a)
    //    where a = PI/2 * R with 0 <= R < 1
    if RELAX < 0.0 || RELAX >= 1.0 {
        panic!("RELAX must be on the interval [0, 1)");
    }
    const COEF_A: f32 = (PI / 2.0) * RELAX;
    (COEF_A * temp).tan() / COEF_A.tan()
}

impl SpeedProfile for Profile {
    fn get_speed(&self, temp_num: CpuTemp) -> Option<FanSpeed> {
        let temp = temp_num as f32;
        let mut max_speed: Option<FanSpeed> = None;
        self.foreach_temp(|comp| {
            if let Some(_) = comp.critical().and_then(|crit_temp| {
                if crit_temp > temp {
                    None
                } else {
                    Some(crit_temp)
                }
            }) {
                return Err(anyhow!(""));
            }

            let max_temp = comp.max();
            if 0.0 == max_temp {
                return Ok(());
            }

            let speed = temp2speed_curve(temp / max_temp);
            max_speed = max_speed.or_else(|| Some(speed));
            if speed > max_speed.unwrap() {
                max_speed = Some(speed);
            }
            Ok(())
        })
        .map_or_else(|_| None, |_| max_speed)
    }
}

impl Display for Profile {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", NAME)
    }
}

impl FanProfile for Profile {}
