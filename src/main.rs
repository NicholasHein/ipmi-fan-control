#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate anyhow;

use args::Command;
use chrono::Local;
use clap::Parser;
use ipmi::{Cmd, FanMode, Ipmi, IpmiTool};
use log::{error, info, warn};
use profiles::{FanProfile, FanSpeed, HumanPercent};
use std::{io::Write, ops::RangeInclusive};
use tokio::time::{self, Duration};

mod args;
mod ipmi;
mod profiles;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args = args::Args::parse();

    let mut level = log::LevelFilter::Debug;

    if args.verbose {
        level = log::LevelFilter::Trace;
    }

    env_logger::Builder::new()
        .format(|buf, record| {
            writeln!(
                buf,
                "{} {} {}",
                Local::now().format("%Y-%m-%dT%H:%M:%S%.3f"),
                record.level(),
                record.args()
            )
        })
        .filter_level(level)
        .init();

    let mut tool = IpmiTool::new(Box::new(Cmd::new()));

    match args.command {
        Command::Auto(a) => {
            let mut interval = a.interval;
            if !RangeInclusive::new(5, 120).contains(&interval) {
                interval = 5;
                info!("invalid interval, interval set to 5");
            }

            let mut threshold = a.threshold;
            if !RangeInclusive::new(60, 100).contains(&threshold) {
                threshold = 70;
                info!("invalid threshold, threshold set to {}", threshold);
            }

            let mut profile: Box<dyn FanProfile> = a.profile.into();

            tool.set_fan_mode(FanMode::Static { auto: true })
                .unwrap_or_else(|err| error!("failed to set fan mode: {}", err));

            info!(
                "auto mode start, interval: {}, threshold: {}",
                interval, threshold
            );

            let mut last_speed = FanSpeed::MIN_PERCENT;
            let mut interval = time::interval(Duration::from_secs(interval));

            loop {
                interval.tick().await;
                let temp_result = tool
                    .get_cpu_temperatures()
                    .and_then(|data| profile.get_temp(&data));
                if let Err(err) = temp_result {
                    error!("failed to read cpu temperature: {}", err);
                    continue;
                }

                let temperature = temp_result.unwrap();
                // transfer temperature to fan speed
                let speed: FanSpeed = profile.get_speed(temperature).unwrap_or_else(|| {
                    warn!("temperature reached threshold {}", temperature);
                    FanSpeed::MAX_PERCENT
                });

                if last_speed == speed {
                    continue;
                }

                tool.set_fan_speed(speed)
                    .and_then(|_| {
                        last_speed = speed;
                        info!(
                            "temperature: {}, set fan speed to {}%",
                            temperature,
                            speed.to_human()
                        );
                        Ok(())
                    })
                    .unwrap_or_else(|err| error!("failed to set fan speed: {}", err));
            }
        }
        Command::Fixed(f) => {
            let v = FanSpeed::from_human(f.value).unwrap_or_else(|err| {
                error!("fan speed input '{}' invalid: {}", f.value, err);
                FanSpeed::MAX_PERCENT
            });

            tool.set_fan_mode(FanMode::Static { auto: false })
                .unwrap_or_else(|err| error!("failed to set fan mode: {}", err));

            info!("fixed mode, set fan speed to {}%", v.to_human());

            if let Err(e) = tool.set_fan_speed(v) {
                error!("set fan speed, error: {}", e);
            }
        }
        Command::Reset => {
            tool.set_fan_mode(FanMode::Dynamic)
                .unwrap_or_else(|err| error!("failed to reset fan mode: {}", err));
        }
        Command::Info => match tool.get_info_fan_temp() {
            Ok(info) => {
                println!("{}", info);
            }
            Err(err) => {
                error!("get info error: {}", err);
            }
        },
    }
}
