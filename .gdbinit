#!/usr/bin/rust-gdb

set print pretty on
set print array on
set print array-indexes on
set print address on
set print symbol on

set print elements unlimited
set print max-depth unlimited

set args auto --interval 5 --threshold 70 --profile poweredge-r610

break main
commands
        silent!
        set scheduler locking on
        continue
end
